// ---------
// Collatz.h
// ---------

#ifndef Collatz_h
#define Collatz_h

// --------
// includes
// --------

#include <iostream> // istream, ostream

using namespace std;

// ------------
// collatz_read
// ------------

/**
 * read two ints
 * @param r an istream
 * @param i an int
 * @param j an int
 * @return r
 */
istream& collatz_read (istream& r, int& i, int& j);

// --------------------
// current_cycle_length
// --------------------

/**
 * Recursively computes cycle length of a given num,
 * storing values as we find them.
 */
unsigned long current_cycle_length(unsigned long cycle_len, unsigned long curr_num);

// ----------
// swap_ints
// ----------

/**
 * Swaps two values
 */
void swap_ints(int& i, int& j);

// ----------------
// initialize_range
// ----------------

/**
 * Sets up the range from lower_bound to upper_bound.
 * Ensures that lower_bound is atleast half of upper_bound,
 * as anything smaller is redundant,
 * based on the logic from Quiz 4, Question 2
 */
void initialize_range(int& lower_bound, int& upper_bound);
// ------------
// collatz_eval
// ------------

/**
 * @param i the beginning of the range, inclusive
 * @param j the end       of the range, inclusive
 * @return the max cycle length of the range [i, j]
 */
int collatz_eval (int i, int j);

// -------------
// collatz_print
// -------------

/**
 * print three ints
 * @param w an ostream
 * @param i the beginning of the range, inclusive
 * @param j the end       of the range, inclusive
 * @param v the max cycle length
 */
void collatz_print (ostream& w, int i, int j, int v);

// -------------
// collatz_solve
// -------------

/**
 * @param r an istream
 * @param w an ostream
 */
void collatz_solve (istream& r, ostream& w);

#endif // Collatz_h
