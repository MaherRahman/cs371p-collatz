// ---------------
// TestCollatz.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <iostream> // cout, endl, istream
#include <sstream>  // istringtstream, ostringstream

#include "gtest/gtest.h"

#include "Collatz.hpp"

using namespace std;

// -----------
// TestCollatz
// -----------

// ----
// read
// ----

TEST(CollatzFixture, read) {
    istringstream r("1 10\n");
    int i;
    int j;
    istream& s = collatz_read(r, i, j);
    ASSERT_TRUE(s);
    ASSERT_EQ(i,  1);
    ASSERT_EQ(j, 10);
}

// ----
// swap
// ----

TEST(CollatzFixture, my_eval_1) {
    int i = 10;
    int j = 1;
    swap_ints(i, j);
    ASSERT_EQ(i, 1);
    ASSERT_EQ(j, 10);
}

// ----------------
// initialize_range
// ----------------

TEST(CollatzFixture, my_eval_2) {
    int i = 10;
    int j = 7;
    initialize_range(i, j);
    ASSERT_EQ(i, 7);
    ASSERT_EQ(j, 10);
}

TEST(CollatzFixture, my_eval_3) {
    int i = 10;
    int j = 1;
    initialize_range(i, j);
    ASSERT_EQ(i, 6);
    ASSERT_EQ(j, 10);
}

TEST(CollatzFixture, my_eval_4) {
    int i = 1;
    int j = 10;
    initialize_range(i, j);
    ASSERT_EQ(i, 6);
    ASSERT_EQ(j, 10);
}

TEST(CollatzFixture, my_eval_5) {
    int i = 1;
    int j = 100;
    initialize_range(i, j);
    ASSERT_EQ(i, 51);
    ASSERT_EQ(j, 100);
}

// --------------------
// current_cycle_length
// --------------------

TEST(CollatzFixture, my_eval_6) {
    unsigned long len = 0;
    unsigned long collatz_num = 5;
    len = current_cycle_length(0, collatz_num);
    ASSERT_EQ(len, 6);
}

TEST(CollatzFixture, my_eval_7) {
    unsigned long len = 0;
    unsigned long collatz_num = 1;
    len = current_cycle_length(0, collatz_num);
    ASSERT_EQ(len, 1);
}

TEST(CollatzFixture, my_eval_8) {
    unsigned long len_1 = 0;
    unsigned long len_2 = 0;
    unsigned long collatz_num_1 = 10;
    unsigned long collatz_num_2 = 5;
    len_1 = current_cycle_length(0, collatz_num_1);
    len_2 = current_cycle_length(0, collatz_num_2);
    ASSERT_EQ(len_1, 7);
    ASSERT_EQ(len_2, 6);
}

// ----
// eval
// ----

TEST(CollatzFixture, eval_1) {
    const int v = collatz_eval(1, 10);
    ASSERT_EQ(v, 20);
}

TEST(CollatzFixture, eval_2) {
    const int v = collatz_eval(100, 200);
    ASSERT_EQ(v, 125);
}

TEST(CollatzFixture, eval_3) {
    const int v = collatz_eval(201, 210);
    ASSERT_EQ(v, 89);
}

TEST(CollatzFixture, eval_4) {
    const int v = collatz_eval(900, 1000);
    ASSERT_EQ(v, 174);
}

TEST(CollatzFixture, my_eval_9) {
    const int v = collatz_eval(10, 1);
    ASSERT_EQ(v, 20);
}

TEST(CollatzFixture, my_eval_10) {
    const int v = collatz_eval(5, 5);
    ASSERT_EQ(v, 6);
}

TEST(CollatzFixture, my_eval_11) {
    const int v = collatz_eval(2, 1);
    ASSERT_EQ(v, 2);
}

TEST(CollatzFixture, my_eval_12) {
    const int v = collatz_eval(2, 1);
    ASSERT_EQ(v, 2);
}

TEST(CollatzFixture, my_eval_13) {
    const int v = collatz_eval(1000000, 1000000);
    ASSERT_EQ(v, 153);
}

TEST(CollatzFixture, my_eval_14) {
    const int v = collatz_eval(1024, 1024);
    ASSERT_EQ(v, 11);
}

// -----
// print
// -----

TEST(CollatzFixture, print) {
    ostringstream w;
    collatz_print(w, 1, 10, 20);
    ASSERT_EQ(w.str(), "1 10 20\n");
}

// -----
// solve
// -----

TEST(CollatzFixture, solve) {
    istringstream r("1 10\n100 200\n201 210\n900 1000\n10 1");
    ostringstream w;
    collatz_solve(r, w);
    ASSERT_EQ("1 10 20\n100 200 125\n201 210 89\n900 1000 174\n10 1 20\n", w.str());
}
