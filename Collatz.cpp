// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream

#include "Collatz.hpp"
using namespace std;

const int MILLION = 1000000;

unsigned long cache[MILLION] = {};


// ------------
// collatz_read
// ------------

istream& collatz_read (istream& r, int& i, int& j) {
    return r >> i >> j;
}

// --------------------
// current_cycle_length
// --------------------

/**
 * Recursively computes cycle length of a given num,
 * storing values as we find them.
 */
unsigned long current_cycle_length(unsigned long cycle_len, unsigned long curr_num) {
    bool inbounds = curr_num < MILLION;
    // If cache[num] already has a non-zero value, then we can return that.
    // if curr_num is 1, then we've finished the cycle, return length of 1.
    if (curr_num == 1 || (inbounds && cache[curr_num])) {
        return max(cache[curr_num], 1ul);
    }
    unsigned long next_num;
    if(curr_num % 2 == 0) {
        next_num = curr_num >> 1;
        assert(next_num == curr_num / 2);
        cycle_len += current_cycle_length(cycle_len, next_num) + 1;
    }
    else {
        // If curr_num is odd, we can actually move twice in the cycle.
        next_num = (((curr_num * 3) + 1) >> 1);
        cycle_len += current_cycle_length(cycle_len, next_num) + 2;
    }
    // If cache[num] doesn't have a non-zero value in it, lets store the length we found.
    assert(cycle_len > 0);
    if(inbounds && !cache[curr_num]) {
        cache[curr_num] = cycle_len;
    }
    return cycle_len;
}

// ----------
// swap_ints
// ----------

/**
 * Swaps two values
 */
void swap_ints(int& i, int& j) {
    // We want to swap the values stored at i and j, so create a temp.
    int temp = i;
    i = j;
    j = temp;
}

// ----------------
// initialize_range
// ----------------

/**
 * Sets up the range from lower_bound to upper_bound.
 * Ensures that lower_bound is atleast half of upper_bound,
 * as anything smaller is redundant,
 * based on the logic from Quiz 4, Question 2
 */
void initialize_range(int& lower_bound, int& upper_bound) {
    // We want the value of lower_bound to be the low end of the range.
    if (lower_bound > upper_bound) {
        swap_ints(lower_bound, upper_bound);
    }
    assert(lower_bound <= upper_bound);
    // We can trim down the range we need to view, logic based on Quiz 4, Question 2.
    lower_bound = max(lower_bound, (upper_bound >> 1) + 1);
    // Again, to make sure our bit shift is valid, we need to make sure i is smaller than j.
    assert(lower_bound <= upper_bound);
}

// ------------
// collatz_eval
// ------------

/**
 * Solves Collatz and returns max cycle length found from range i to j.
 * @param  lower_bound start of the range
 * @param  upper_bound end of the range
 * @return The max cycle length over the range
 */
int collatz_eval (int lower_bound, int upper_bound) {
    unsigned long max_cycle_len = 0;
    initialize_range(lower_bound, upper_bound);
    assert(lower_bound <= upper_bound);
    for(int index = lower_bound; index <= upper_bound; ++index) {
        unsigned long curr_cycle_len = current_cycle_length(0, index);
        assert(curr_cycle_len > 0);
        max_cycle_len = max(curr_cycle_len, max_cycle_len);
        assert(max_cycle_len >= curr_cycle_len);
    }
    assert(max_cycle_len > 0);
    return max_cycle_len;
}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& w, int i, int j, int v) {
    w << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& r, ostream& w) {
    int i;
    int j;
    while (collatz_read(r, i, j)) {
        const int v = collatz_eval(i, j);
        collatz_print(w, i, j, v);
    }
}