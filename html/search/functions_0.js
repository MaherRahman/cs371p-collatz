var searchData=
[
  ['collatz_5feval',['collatz_eval',['../Collatz_8cpp.html#a6a8232f5f1918cf396127b78512307ff',1,'collatz_eval(int lower_bound, int upper_bound):&#160;Collatz.cpp'],['../Collatz_8hpp.html#a0b0d3827a619c18aa4d96b8ee8b1c47d',1,'collatz_eval(int i, int j):&#160;Collatz.cpp']]],
  ['collatz_5fprint',['collatz_print',['../Collatz_8cpp.html#aeda0b7ea3e40e1e7487ccc436f33a559',1,'collatz_print(ostream &amp;w, int i, int j, int v):&#160;Collatz.cpp'],['../Collatz_8hpp.html#aeda0b7ea3e40e1e7487ccc436f33a559',1,'collatz_print(ostream &amp;w, int i, int j, int v):&#160;Collatz.cpp']]],
  ['collatz_5fread',['collatz_read',['../Collatz_8cpp.html#aa0019f52451ebd3c129529f7ee0fd002',1,'collatz_read(istream &amp;r, int &amp;i, int &amp;j):&#160;Collatz.cpp'],['../Collatz_8hpp.html#aa0019f52451ebd3c129529f7ee0fd002',1,'collatz_read(istream &amp;r, int &amp;i, int &amp;j):&#160;Collatz.cpp']]],
  ['collatz_5fsolve',['collatz_solve',['../Collatz_8cpp.html#a0ac646d2122741f9a9a52201bf9551cc',1,'collatz_solve(istream &amp;r, ostream &amp;w):&#160;Collatz.cpp'],['../Collatz_8hpp.html#a0ac646d2122741f9a9a52201bf9551cc',1,'collatz_solve(istream &amp;r, ostream &amp;w):&#160;Collatz.cpp']]],
  ['current_5fcycle_5flength',['current_cycle_length',['../Collatz_8cpp.html#a08467c1adb736b048d7cd832ff0ee256',1,'current_cycle_length(unsigned long cycle_len, unsigned long curr_num):&#160;Collatz.cpp'],['../Collatz_8hpp.html#a08467c1adb736b048d7cd832ff0ee256',1,'current_cycle_length(unsigned long cycle_len, unsigned long curr_num):&#160;Collatz.cpp']]]
];
