# CS371p: Object-Oriented Programming Collatz Repo

* Name: Maher Rahman

* EID: mmr3447

* GitLab ID: MaherRahman

* HackerRank ID: MaherRahman1

* Git SHA: 32f95c931965d608c955cc8cb5305f582ab19f12

* GitLab Pipelines: https://gitlab.com/MaherRahman/cs371p-collatz/pipelines

* Estimated completion time: 8 hours

* Actual completion time: 10 hours

* Comments: The assignment was not as difficult as I originally thought. Using a lazy cache was sufficient to solving Collatz, and even using an array with 1,000,000 entries was still sufficient. I was a bit worried in class about not timing out, but as I realized recursively solving Collatz was much faster than iteratively, my life became sweet. The main concern was all of the other things we had to do
involving Gitlab and our workflow. It felt excessive for such a small-scale project such as Collatz. 
